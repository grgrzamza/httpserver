﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace HTTP_Server
{
    class Interaction
    {
        const string Server_Directory = "C:\\FtpServer\\";
        const string Error_Message = "Empty";
        const string Main_Page = "Main.txt";

        TcpClient Client;
        Hashtable Contents = new Hashtable();

        public Interaction(TcpClient client)
        {
            Client = client;
            SetContents();
            Thread interact = new Thread(new ThreadStart(Interact));
            interact.Start();
        }

        public string GetPath(string request)
        {
            int space1 = request.IndexOf(" ");
            int space2 = request.IndexOf(" ", space1 + 1);
            string url = request.Substring(space1 + 2, space2 - space1 - 2);
            if (url == "")
                url = Main_Page;
            return Server_Directory + url;
        }

        public string GetContent(string file_path)
        {
            string ext = "";
            int dot = file_path.LastIndexOf(".");
            if (dot >= 0)
                ext = file_path.Substring(dot, file_path.Length - dot).ToUpper();
            if (Contents[ext] == null)
                return "application/" + ext.Substring(1,ext.Length-1);
            else
                return (string)Contents[ext];
        }

        public void WriteHeaderToClient(string content_type, long length)
        {
            string str = "HTTP/1.1 200 OK\nContent-type: " + content_type + "\nContent-Length:" + length.ToString() + "\n\n";
            Client.GetStream().Write(Encoding.ASCII.GetBytes(str), 0, str.Length);
        }

        public void WriteToClient(string request)
        {
            string file_path = GetPath(request);
            if (file_path.IndexOf("..") >= 0 || !File.Exists(file_path))
            {
                WriteHeaderToClient("text/plain", Error_Message.Length);
                Client.GetStream().Write(Encoding.ASCII.GetBytes(Error_Message), 0, Error_Message.Length);
                return;
            }
            FileStream file = File.Open(file_path, FileMode.Open);
            WriteHeaderToClient(GetContent(file_path), file.Length);
            byte[] buf = new byte[1024];
            int len;
            while ((len = file.Read(buf, 0, 1024)) != 0)
                Client.GetStream().Write(buf, 0, len);
            file.Close();
        }
        public void Interact()
        {
                byte[] buffer = new byte[1024];
                string request = "";
                while (true)
                {
                    int count = Client.GetStream().Read(buffer, 0, 1024);
                    request += Encoding.ASCII.GetString(buffer, 0, count);
                    if (request.IndexOf("\r\n\r\n") >= 0) 
                    {
                        WriteToClient(request);
                        request = "";
                    }
                }
        }
        protected void SetContents()
        {
            Contents.Add("", "application/unknown");
            Contents.Add(".PDF", "application/pdf");
            Contents.Add(".HTML", "text/html");
            Contents.Add(".HTM", "text/html");
            Contents.Add(".TXT", "text/plain");
            Contents.Add(".GIF", "image/gif");
            Contents.Add(".JPG", "image/jpeg");
            Contents.Add(".PNG", "image/png");
            Contents.Add(".CSS", "text/css");
            Contents.Add(".PHP", "text/php");
        }
        
    }
}
