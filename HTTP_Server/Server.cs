﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace HTTP_Server
{
    class Server
    {
        private TcpListener Listener;

        public Server(int port)
        {
            Listener = new TcpListener(port);
            Listener.Start();
            Listen();
        }
        
        public void Listen()
        {
            while (true)
            {
                TcpClient client = Listener.AcceptTcpClient();
                Interaction proccess = new Interaction(client);
            }
        }

    }
}
